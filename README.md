### DIMC Compiled SW Repository ###

This repository contains the following files that are compiled from the DIMC SW source code:

- .hex file: This is the binary file that can be flashed to the microcontroller.
- .map file: This is the file that shows the memory layout of the program.
- .list file: This is the file that contains the assembly code and the source code line by line.
- .elf file: This is the executable file that can be debugged using a debugger tool.

You can download these files from the [download folder](https://bitbucket.org/ips-software-team/dimc_build_release/downloads/) in this repository.